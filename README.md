# Adobe Photoshop

[[_TOC_]]

> pho·to·shop, transitive verb, often capitalized ˈfō-(ˌ)tō-ˌshäp to alter (a
> digital image) with Photoshop software or other image-editing software
> especially in a way that distorts reality (as for deliberately deceptive
> purposes)
>
> — Merriam-Webster Online Dictionary, 2012

When brothers Thomas and John Knoll began designing and writing an image
editing program in the late 1980s, they could not have imagined that they
would be adding a word to the dictionary.

---

![](assets/photoshop-source-code-thomas-knoll.jpg){width=25%}

*Thomas Knoll*

---

![](assets/photoshop-source-code-john-knoll.jpg){width=25%}

*John Knoll*

---

Thomas Knoll, a PhD student in computer vision at the University of Michigan,
had written a program in 1987 to display and modify digital images. His
brother John, working at the movie visual effects company Industrial Light &
Magic, found it useful for editing photos, but it wasn’t intended to be a
product. Thomas said, “We developed it originally for our own personal use…it
was a lot a fun to do.”

Gradually the program, called “Display”, became more sophisticated. In the
summer of 1988 they realized that it indeed could be a credible commercial
product. They renamed it “Photoshop” and began to search for a company to
distribute it. About 200 copies of version 0.87 were bundled by slide scanner
manufacturer Barneyscan as “Barneyscan XP”.

The fate of Photoshop was sealed when Adobe, encouraged by its art director
Russell Brown, decided to buy a license to distribute an enhanced version of
Photoshop. The deal was finalized in April 1989, and version 1.0 started
shipping early in 1990.

Over the next ten years, more than 3 million copies of Photoshop were sold.

---

![](assets/photoshop-source-code-box-and-disk.jpg){width=25%}

*The box and disk for the original version of Photoshop on Mac.*

---

That first version of Photoshop was written primarily in Pascal for the Apple
Macintosh, with some machine language for the underlying Motorola 68000
microprocessor where execution efficiency was important. It wasn’t the effort
of a huge team. Thomas said, “For version 1, I was the only engineer, and for
version 2, we had two engineers.” While Thomas worked on the base application
program, John wrote many of the image-processing plug-ins.

---

![](assets/photoshop-source-code-splashscreen.jpg){width=25%}

*The splash screen of Photoshop 1.0.7.*

---

## Download

[Photoshop version 1.0.1 Source Code](src)

- [1990 version of Adobe Photoshop User Guide](assets/102640940-05-01-acc.pdf)
- [1990 version of Adobe Photoshop tutorial](assets/102640945-05-01-acc.pdf)

## Commentary on the source code

Software architect Grady Booch is the Chief Scientist for Software Engineering
at IBM Research Almaden and a trustee of the Computer History Museum. He
offers the following observations about the Photoshop source code:

- “Opening the files that constituted the source code for Photoshop 1.0, I
  felt a bit like Howard Carter as he first breached the tomb of King
  Tutankhamen. What wonders awaited me?

- I was not disappointed by what I found. Indeed, it was a marvelous journey
  to open up the cunning machinery of an application I’d first used over 20
  years ago.

- Architecturally, this is a very well-structured system. There’s a consistent
  separation of interface and abstraction, and the design decisions made to
  componentize those abstractions – with generally one major type for each
  combination of interface and implementation – were easy to follow.

- The abstractions are quite mature. The consistent naming, the granularity of
  methods, the almost breathtaking simplicity of the implementations because
  each type was so well abstracted, all combine to make it easy to discern the
  texture of the system.

- Having the opportunity to examine Photoshop’s current architecture, I
  believe I see fundamental structures that have persisted, though certainly
  in more evolved forms, in the modern implementation. Tiles, filters,
  abstractions for virtual memory (to attend to images far larger than display
  buffers or main memory could normally handle) are all there in the first
  version. Yet it had just over 100,000 lines of code, compared to well over
  10 million in the current version! Then and now, much of the code is related
  to input/output and the myriad of file formats that Photoshop has to attend
  to.

- There are only a few comments in the version 1.0 source code, most of which
  are associated with assembly language snippets. That said, the lack of
  comments is simply not an issue. This code is so literate, so easy to read,
  that comments might even have gotten in the way.

- It is delightful to find historical vestiges of the time: code to attend to
  Andy Herzfield’s software for the Thunderscan scanner, support of early
  TARGA raster graphics file types, and even a few passing references to
  Barneyscan lie scattered about in the code. These are very small elements of
  the overall code base, but their appearance reminds me that no code is an
  island.

- This is the kind of code I aspire to write.”

And this is the kind of code we all can learn from. Software source code is
the literature of computer scientists, and it deserves to be studied and
appreciated. Enjoy a view of Photoshop from the inside.

## Early Photoshop screenshots

---

![](assets/photoshop-source-code-screen-main.jpg){width=25%}

*The home screen, showing the available tools.*

---

![](assets/photoshop-source-code-screen-brushes.jpg){width=25%}

*Photoshop allowed you to select brush colour as well as size and
texture. (The first colour Mac was the Macintosh II in 1987.)*

---

![](assets/photoshop-source-code-screen-image_filters.jpg){width=25%}

*There were some sophisticated tools, and a good assortment of image
filters. One important missing feature, which came with version 3 in 1994, was
the ability to divide an image into multiple layers.*

---

![](assets/photoshop-source-code-screen-preferences.jpg){width=25%}

*The preferences page allowed for some customisation of the features.*

---

![](assets/photoshop-source-code-screen-fonts.jpg){width=25%}

*There was a limited choice of fonts, font sizes, and font styles. The text
was entered into this dialog box, then moved into the image.*

---
